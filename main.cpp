#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "vector"

int truncate(int a) {
    if (a < 0) {
        return 0;
    }
    if (a > 255) {
        return 255;
    }
    return a;
}

cv::Vec3b resultPixelHighPass(cv::Mat &image, int iResult, int jResult) {
    int r = 0;
    int g = 0;
    int b = 0;
    for (int i = -2; i <= 2; i++) {
        for (int j = -2; j <= 2; j++) {
            cv::Vec3b color = image.at<cv::Vec3b>(cv::Point(iResult + i, jResult + j));
            if (i == 0 && j == 0) {
                r += color[0] * 24;
                g += color[1] * 24;
                b += color[2] * 24;

            } else {
                r -= color[0];
                g -= color[1];
                b -= color[2];
            }
        }
    }
    r = truncate(r);
    g = truncate(g);
    b = truncate(b);
    return cv::Vec3b(r, g, b);
}

cv::Vec3b resultPixelLowPass(cv::Mat &image, int iResult, int jResult) {
    int r = 0;
    int g = 0;
    int b = 0;
    for (int i = -2; i <= 2; i++) {
        for (int j = -2; j <= 2; j++) {
            cv::Vec3b color = image.at<cv::Vec3b>(cv::Point(iResult + i, jResult + j));
            r += color[0] / 25;
            g += color[1] / 25;
            b += color[2] / 25;
        }
    }
    return cv::Vec3b(r, g, b);
}

cv::Mat lowPass(cv::Mat &image) {
    cv::Mat result = image.clone();
    for (int i = 2; i < image.rows - 2; ++i) {
        for (int j = 2; j < image.cols - 2; ++j) {
            result.at<cv::Vec3b>(cv::Point(i, j)) = resultPixelLowPass(image, i, j);
        }
    }
    return result;
}

cv::Mat highPass(cv::Mat &image) {
    cv::Mat result = image.clone();
    for (int i = 2; i < image.rows - 2; ++i) {
        for (int j = 2; j < image.cols - 2; ++j) {
            result.at<cv::Vec3b>(cv::Point(i, j)) = resultPixelHighPass(image, i, j);
        }
    }
    return result;
}

bool comparator(cv::Vec3b a, cv::Vec3b b) {
    return (a[0] / 3 + a[1] / 3 + a[2] / 3) > (b[0] / 3 + b[1] / 3 + b[2] / 3);
}

cv::Vec3b resultPixelRankFilter(cv::Mat &image, int iResult, int jResult, int n, int index) {
    std::vector<cv::Vec3b> pixelVector;
    for (int i = -n / 2; i <= n / 2; i++) {
        for (int j = -n / 2; j <= n / 2; j++) {
            cv::Vec3b color = image.at<cv::Vec3b>(cv::Point(iResult + i, jResult + j));
            pixelVector.push_back(color);
        }
    }
    std::sort(pixelVector.begin(), pixelVector.end(), comparator);
    return pixelVector[index];
}

cv::Mat rankFilter(cv::Mat &image) {
    int n;
    int index;
    std::cout << "Rozmiar filtru: ";
    std::cin >> n;
    std::cout << std::endl << "Indeks filtru: ";
    std::cin >> index;
    std::cout << std::endl;
    cv::Mat result = image.clone();
    for (int i = n / 2; i < image.rows - n / 2; ++i) {
        for (int j = n / 2; j < image.cols - n / 2; ++j) {
            result.at<cv::Vec3b>(cv::Point(i, j)) = resultPixelRankFilter(image, i, j, n, index);
        }
    }
    return result;
}


int main() {
    cv::Mat image = cv::imread("lena.png");
    cv::Mat resultLowPass = lowPass(image);
    cv::Mat resultHighPass = highPass(image);
    cv::Mat resultRankFilter = rankFilter(image);
    cv::imwrite("lena_low.png", resultLowPass);
    cv::imwrite("lena_high.png", resultHighPass);
    cv::imwrite("lena_rank.png", resultRankFilter);
    return 0;
}
